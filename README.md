Hi,

here a PoC of a Module which implements the requirement for the Excercice provided.

I concentrated on the actual implementation of the idea rather than style and elegance, so class names are not 100% consistent, code styling is not the best I've ever written, DB types are also not quite accurate and there is no logging. Also I left out tests for the purpose of this exercice.

The module in a nutshell implement a Magento 2 Admin Grid which shows values of product stock variation collected via an Observer, the link will be under the menu Products > Product Qty Checker.

In this Grid, displayed are the values from the DB table 'product_qty_checker'. This one is also created via an Install Schema within the module.

The core of the implementation is in a serie of Observers, each specialized for a specific area (Admin, Frontend, Cron, API). They all extend an Abstract Class which implement the logic for the required task.

the Observers are listening to the event 'catalog_product_save_after' this is the first of the possible weaknesses once the site will be under heavy load as it will run the code in the Observer every time the product is saved.

I'd have preferred using 'cataloginventory_stock_item_save_before' which would have been a bit more efficient, this was not possible due to this known Magento issue:

https://github.com/magento/magento2/issues/4857

I didn't solve/try to workaround the issue as I though it would have been out of topic for the exercice, so I used 'catalog_product_save_after' instead.

Another possible flaw in the implementation is the necessity of loading the collection of the previously saved product qty checks to retrive the last product quantity saved, in order to calculate the difference (variation) this could have possibly been avoided using the alternative event.

Connected to the last one an improvement could be creating an index on table 'product_qty_checker' for the filed product_id for better performances in searches.

Also using an Observer to listen to product_save or even cataloginventory_stock_item_save could load the checkout steps which could result in a risk during heavy load.

Module was developed on 
Magento ver. 2.1.7,
DB: 10.1.13-MariaDB,
Apache: 2.4.17,
PHP version: 5.6.21