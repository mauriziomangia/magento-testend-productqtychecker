<?php
namespace Testend\Productqtychecker\Model;
use Magento\Framework\Exception\LocalizedException as CoreException;
class Productqtychecker extends \Magento\Framework\Model\AbstractModel
{
	
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Testend\Productqtychecker\Model\ResourceModel\Productqtychecker');
    }
}