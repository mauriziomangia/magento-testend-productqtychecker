<?php
namespace Testend\Productqtychecker\Model\ResourceModel\Productqtychecker;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Testend\Productqtychecker\Model\Productqtychecker', 'Testend\Productqtychecker\Model\ResourceModel\Productqtychecker');
    }
}