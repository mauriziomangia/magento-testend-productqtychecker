<?php
namespace Testend\Productqtychecker\Model\ResourceModel;

class Productqtychecker extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	/**
	 * Initialize resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('product_qty_checker', 'entity_id');
	}
}