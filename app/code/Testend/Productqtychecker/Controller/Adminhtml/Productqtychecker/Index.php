<?php

namespace Testend\Productqtychecker\Controller\Adminhtml\Productqtychecker;
use Magento\Framework\Controller\ResultFactory;
class Index extends \Testend\Productqtychecker\Controller\Adminhtml\Productqtychecker
{
    public function execute()
    {
       $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()->prepend(__('Product Qty Checker'));
        return $resultPage;
    }
}