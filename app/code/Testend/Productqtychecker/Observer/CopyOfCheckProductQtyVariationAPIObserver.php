<?php
namespace Testend\Productqtychecker\Observer;

use Testend\Productqtychecker\Observer\CheckProductQtyVariationObserver;

class CheckProductQtyVariationAPIObserver extends CheckProductQtyVariationObserver
{
	
	/**
	 * constructor.
	 *
	 */
	public function __construct()
	{
		$this->action = "API";
	}
	
}