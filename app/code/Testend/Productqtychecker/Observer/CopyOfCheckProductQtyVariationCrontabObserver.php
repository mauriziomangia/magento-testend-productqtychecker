<?php
namespace Testend\Productqtychecker\Observer;

use Testend\Productqtychecker\Observer\CheckProductQtyVariationObserver;

class CheckProductQtyVariationCrontabObserver extends CheckProductQtyVariationObserver
{
	
	/**
	 * constructor.
	 *
	 */
	public function __construct()
	{
		$this->action = "Crontab";
	}
	
}