<?php 
namespace Testend\Productqtychecker\Observer;

use Magento\Framework\Event\ObserverInterface;

abstract class CheckProductQtyVariationObserver implements ObserverInterface
{
	protected $action;
	
	

  public function execute(\Magento\Framework\Event\Observer $observer)
  {
  	
  	/** @var Product $product */
  	$product = $observer->getEvent()->getProduct();
  	
  	
  	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
  	
  	$StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
  	$productQty = $StockState->getStockQty($product->getId(), $product->getStore()->getWebsiteId());
  	
  	
  	$previousProductQtyChecker= $objectManager
  		->create('Testend\Productqtychecker\Model\ResourceModel\Productqtychecker\Collection')
  		->addFieldToFilter('product_id', $product->getId())
  		->setOrder('created_at','desc')
  		->getFirstItem();
  	
  	
  	$qtyVariation = 0;
  	if($previousProductQtyChecker->getEntityId()){
  		$previousQty = $previousProductQtyChecker->getQty();
  		
  		$qtyVariation = $productQty - $previousQty;
  	}
  	
  	$productQtyChecker = $objectManager->get('\Testend\Productqtychecker\Model\Productqtychecker');
  	
  	$productQtyChecker
  	->setProductId($product->getId())
  	->setProductSku($product->getSku())
  	->setQty($productQty)
  	->setQtyVariation($qtyVariation)
  	->setAction($this->action)
  	->save();
  	
  	
  }
}