<?php
namespace Testend\Productqtychecker\Observer;

use Testend\Productqtychecker\Observer\CheckProductQtyVariationObserver;

class CheckProductQtyVariationFrontendObserver extends CheckProductQtyVariationObserver
{
	
	/**
	 * constructor.
	 *
	 */
	public function __construct()
	{
		$this->action = "Frontend";
	}
	
}