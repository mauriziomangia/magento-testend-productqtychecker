<?php
namespace Testend\Productqtychecker\Observer;

use Testend\Productqtychecker\Observer\CheckProductQtyVariationObserver;

class CheckProductQtyVariationAdminObserver extends CheckProductQtyVariationObserver
{
	
	/**
	 * constructor.
	 *
	 */
	public function __construct()
	{
		$this->action = "Admin";
	}
	
}