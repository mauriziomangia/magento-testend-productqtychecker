<?php

namespace Testend\Productqtychecker\Block\Adminhtml\Productqtychecker;

class Grid extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'Testend_Productqtychecker';
        $this->_controller = 'adminhtml_productqtychecker';
        $this->_headerText = __('productqtychecker');
        parent::_construct();
        $this->buttonList->remove('add');
    }
}